#![feature(proc_macro_hygiene, decl_macro, try_trait)]

#[macro_use] extern crate lazy_static;
extern crate rand;

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

extern crate postgres;

mod db;
mod dtos;
mod rest;
mod utils;
mod security;
mod services;
mod persistencia;

#[cfg(test)] mod tests;

type BoxResult<T> = std::result::Result< T, Box<std::error::Error> >;

fn main() {
    let mut rocket = rocket::ignite();

	rocket = rest::mount(rocket);
	rocket = db::attach (rocket);

	rocket.launch();
}
