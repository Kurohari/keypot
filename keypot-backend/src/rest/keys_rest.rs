use rocket::Rocket;

use rocket_contrib::json::{Json};

use ::dtos::key::{KeyMessage};
use ::security::{AuthToken};
use ::services::{keys_service};
use ::db::{BackendDbConn};

#[get("/keys")]
pub fn obtener_keys_de_usuario(conn: BackendDbConn, token: AuthToken) -> Json< Vec<KeyMessage> > {
	let keys = keys_service::buscar_keys_por_usuario(&(*conn), token.owner);
	Json(keys)
}


pub fn mount(rocket: Rocket) -> Rocket {
	return rocket.mount("/", routes![obtener_keys_de_usuario]);
}
