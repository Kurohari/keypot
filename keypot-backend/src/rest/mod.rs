pub mod usuarios_rest;
pub mod keys_rest;

use rocket::{Rocket};

use rocket_contrib::json::*;

use ::db::{BackendDbConn};

use ::services::*;

#[derive(Deserialize, Debug)]
pub struct LoginMessage {
	pub email : String,
	pub digest: String
}

#[derive(Serialize, Debug)]
pub struct AuthTokenPublicInfo {
	pub owner : i64,
	pub secret: String
}

#[post("/login", data = "<msg>")]
pub fn login(conn: BackendDbConn, msg: Json<LoginMessage>) -> Result<Json<AuthTokenPublicInfo>, ()> {
	//TODO: [fpalacios] Reimplementar usando tokens de sesion en vez de tokens de autenticacion
	//TODO: [fpalacios] Retornar metodo de error
	let token_result = authservice::login(&*conn, &*msg);
	let token = token_result?;

	let public_token = AuthTokenPublicInfo {
		owner : (*token.owner).id,
		secret: token.secret
	};

	return Ok( Json(public_token) );
}


pub fn mount(mut rocket: Rocket) -> Rocket {
	rocket = rocket.mount("/", routes![login]);
	return usuarios_rest::mount(rocket);
}
