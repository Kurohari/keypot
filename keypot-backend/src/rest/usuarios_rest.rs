use rocket::{Rocket};
use rocket::response::status;

use rocket_contrib::json::{Json};

use ::db::{BackendDbConn};
use ::services::{usuarios_service};
use ::security::{AuthToken};
use ::dtos::usuario::{NuevoUsuario};

#[post("/registro", data = "<nuevo_usuario>")]
pub fn alta_usuario(conn: BackendDbConn, nuevo_usuario: Json<NuevoUsuario>)
	-> Result< Json<i64>, status::BadRequest<String> >
{
	let user_res = usuarios_service::registrar_usuario( &*conn, nuevo_usuario.into_inner() );

	if user_res.is_ok() {
		return Ok( Json( user_res.unwrap().id ) );
	}

	return Err( status::BadRequest( Some( user_res.unwrap_err().description().to_owned() ) ) );
}

#[get("/greet")]
pub fn index(token: AuthToken) -> String {
    return format!("Bienvenido a keypot {}!!!", token.owner.as_ref().email);
}

pub fn mount(rocket: Rocket) -> Rocket {
	return rocket.mount("/", routes![index, alta_usuario]);
}
