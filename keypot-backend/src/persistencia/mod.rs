pub mod usuarios_dao;
pub mod keys_dao;

use std::rc::Rc;

use postgres::Connection;

use ::security::AuthToken;

use ::dtos::usuario::{Usuario};

impl AuthToken {
	pub fn guardar(conn: &Connection, token: &mut AuthToken) {
		//TODO: [fpalacios] Manejar errores que pueda dar la bd

		if token.id.is_none() {
			conn.execute(
				"INSERT INTO auth_tokens(owner, secret) VALUES($1, $2)",
				&[ &(*token.owner).id, &token.secret ]
			).unwrap();
		} else {
			conn.execute(
				"UPDATE auth_tokens SET owner = $1, secret = $2 WHERE id_auth_token = $3",
				&[ &(*token.owner).id, &token.secret, &token.id.unwrap() ]
			).unwrap();
		}

		//TODO: [fpalacios] actualizar el id de el token al insertar/actualiar en la bd
	}

	pub fn buscar_por_usuario(conn: &Connection, usuario: Rc<Usuario>) -> Vec< AuthToken > {
		//TODO: [fpalacios] Manejar errores que pueda dar la bd
		let mut tokens: Vec<AuthToken> = Vec::new();

		let rows = conn.query(
			"SELECT * FROM auth_tokens WHERE owner = $1",
			&[ &usuario.id ]
		);
		for row in &rows.unwrap() {
			let token = AuthToken {
				id    : row.get("id_auth_token"),
				owner : usuario.clone(),
				secret: row.get("secret")
			};

			tokens.push(token);
		}

		return tokens;
	}
}
