use postgres::{Connection};

use ::dtos::usuario::{Usuario, NuevoUsuario};
use ::{BoxResult};


impl NuevoUsuario {
	pub fn insertar(self, conn: &Connection) -> BoxResult<Usuario> {
		let smtp = conn.prepare("INSERT INTO usuarios(email, digest) VALUES($1, $2) RETURNING id_usuario")?;
		let id: i64 = smtp.query(&[&self.email, &self.digest])?
			.iter()
			.next()
			.ok_or("Usuario rechazado por la base de datos")?
			.get("id_usuario");

		return Ok( Usuario{id: id, email: self.email, digest: self.digest} );
	}
}

impl Usuario {
	pub fn buscar_por_email(conn: &Connection, email: &str) -> Option<Usuario> {
		//TODO: [fpalacios] Manejar errores que pueda dar la bd
		let rows = conn.query("SELECT * FROM usuarios where email = $1", &[&email]).unwrap();

		if rows.len() < 1 {
			return Option::None;
		}

		let row = rows.get(0);

		return Option::Some( Usuario {
			id    : row.get("id_usuario"),
			email : email.to_owned(),
			digest: row.get("digest")
		});
	}

	pub fn buscar_por_id(conn: &Connection, id: i64) -> Option<Usuario> {
		//TODO: [fpalacios] Manejar errores que pueda dar la bd
		let rows = conn.query("SELECT * FROM usuarios where id_usuario = $1", &[&id]).unwrap();

		if rows.len() != 1 {
			return Option::None;
		}

		let row = rows.get(0);

		return Option::Some( Usuario {
			id    : id,
			email : row.get("email"),
			digest: row.get("digest")
		});
	}
}
