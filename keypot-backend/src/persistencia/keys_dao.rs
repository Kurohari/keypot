use std::rc::Rc;

use postgres::{Connection};

use ::dtos::key::{Key, NuevaKey};
use ::dtos::usuario::{Usuario};

use ::{BoxResult};

impl NuevaKey {
	pub fn insertar(self, conn: &Connection) -> BoxResult<Key> {
		let rows = conn.query(
			"INSERT INTO keys(url, encod, id_usuario) values($1, $2, $3) RETURNING id_key",
			&[&self.encod, &self.url]
		)?;

		let row = rows.get(0);

		return Ok( Key {
			id     : row.get("id_key"),
			url    : self.url,
			encod  : self.encod,
			usuario: self.usuario
		});
	}
}

impl Key {
	pub fn buscar_por_usuario(conn: &Connection, usuario: Rc<Usuario>) -> BoxResult< Vec<Key> > {
		let rows = conn.query(
			"SELECT id_key, url, encod FROM keys where id_usuario $1",
			&[&(*usuario).id]
		)?;

		let mut keys = Vec::new();

		for row in rows.iter() {
			let key = Key {
				id     : row.get("id_key"),
				url    : row.get("url"),
				encod  : row.get("encod"),
				usuario: usuario.clone()
			};

			keys.push(key);
		}

		return Ok(keys);
	}
}
