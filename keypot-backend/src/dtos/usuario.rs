#[derive(Serialize, Debug)]
pub struct Usuario {
	pub id    : i64,
	pub email : String,
	pub digest: String
}

#[derive(Deserialize, Debug)]
pub struct NuevoUsuario {
	pub email : String,
	pub digest: String
}
