use std::rc::Rc;

use ::dtos::usuario::{Usuario};

pub struct Key {
	pub id     : i64,
	pub url    : String,
	pub encod  : Vec<u8>,
	pub usuario: Rc<Usuario>
}

impl Key {
	pub fn to_key_message(self) -> KeyMessage {


		return KeyMessage {
			id   : self.id,
			url  : self.url,
			encod: String::new()
		};
	}
}

pub struct NuevaKey {
	pub url    : String,
	pub encod  : Vec<u8>,
	pub usuario: Rc<Usuario>
}

#[derive(Deserialize, Serialize)]
pub struct KeyMessage {
	pub id   : i64,
	pub url  : String,
	pub encod: String
}
