use postgres;
use rocket::Rocket;

#[database("backend_db")]
pub struct BackendDbConn(postgres::Connection);


pub fn attach(rocket: Rocket) -> Rocket {
	return rocket.attach( BackendDbConn::fairing() );
}
