use std::rc::Rc;
use std::sync::Mutex;

use rand::Rng;
use rand::rngs::OsRng;
use rand::distributions::Alphanumeric;

use postgres::Connection;

use ::rest::LoginMessage;
use ::security::{AuthToken};
use ::dtos::usuario::{Usuario};

lazy_static! {
	static ref RANDOM: Mutex<OsRng> = Mutex::new( OsRng::new().unwrap() );
}

fn crear_nuevo_auth_token(conn: &Connection, usuario: Usuario) -> AuthToken {
	//TODO: [fpalacios] Manejar errores que pueda dar la capa de persistencia
	let mut token = AuthToken::new( Rc::new(usuario), generate_secure_secret() );

	AuthToken::guardar(conn, &mut token);

	return token;
}

fn generate_secure_secret() -> String {
	let rand = &mut *RANDOM.lock().unwrap();

	let secret: String = rand
		.sample_iter(&Alphanumeric)
		.take(64)
		.collect();

	return secret;
}

pub fn login(conn: &Connection, msg: &LoginMessage) -> Result<AuthToken, ()> {
	let usuario_result = Usuario::buscar_por_email(conn, &msg.email);

	if usuario_result.is_none() {
		return Result::Err(());
	}

	let usuario = usuario_result.unwrap();

	if &usuario.digest != &msg.digest {
		return Result::Err(());
	}

	let token = crear_nuevo_auth_token(conn, usuario);

	return Result::Ok(token);
}

pub fn validar_auth(conn: &Connection, id_user: i64, secret: String) -> Result<AuthToken, ()> {
	let usuario = Usuario::buscar_por_id(&conn, id_user);

	if usuario.is_none() {
		return Result::Err(());
	}

	let tokens_usuario = AuthToken::buscar_por_usuario( &conn, Rc::new( usuario.unwrap() ) );

	for token in tokens_usuario {
		if token.secret == secret {
			return Result::Ok(token);
		}
	}

	return Result::Err(());
}
