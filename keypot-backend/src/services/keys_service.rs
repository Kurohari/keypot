use std::rc::{Rc};

use postgres::{Connection};

use ::dtos::usuario::{Usuario};
use ::dtos::key::{Key};

pub fn buscar_keys_por_usuario(conn: &Connection, usuario: Rc<Usuario>) -> Vec<Key> {
	return Key::buscar_por_usuario(conn, usuario).unwrap();
}
