use dtos::usuario::{Usuario, NuevoUsuario};

use postgres::Connection;

use ::BoxResult;

pub fn registrar_usuario(conn: &Connection, nuevo_usuario: NuevoUsuario) -> BoxResult<Usuario> {
	if Usuario::buscar_por_email(conn, &nuevo_usuario.email).is_some() {
		Err("El usuario ya existe")?;
	}

	return Ok(nuevo_usuario.insertar(&conn)?);
}
