use std::rc::Rc;

use rocket::*;
use rocket::request::FromRequest;

use rocket::http::Status;
use rocket::outcome::Outcome;

use ::services::authservice;
use ::db::BackendDbConn;
use ::dtos::usuario::{Usuario};

pub struct AuthToken {
	pub id    : Option<i32>,
	pub owner : Rc<Usuario>,
	pub secret: String
}

impl AuthToken {
	pub fn new(owner: Rc<Usuario>, secret: String) -> AuthToken {
		return AuthToken {
			id:     Option::None,
			owner:  owner,
			secret: secret
		};
	}
}

impl<'a, 'r> FromRequest<'a, 'r> for AuthToken {
	type Error = ();

	fn from_request(request: &'a Request<'r>) -> request::Outcome< AuthToken, () > {

		let auth_user_field   = request.headers().get_one("auth-user");
		let auth_secret_field = request.headers().get_one("auth-secret");

		if auth_user_field.is_none()  {
			return Outcome::Failure((Status::BadRequest, ()));
		}

		let id_user = match auth_user_field.unwrap().parse::<i64>() {
			Ok(num) => num,
			Err(_)  => return Outcome::Failure((Status::BadRequest, ()))
		};

		if auth_secret_field.is_none() {
			return Outcome::Failure((Status::BadRequest, ()));
		}

		let secret = auth_secret_field.unwrap().to_owned();

		let conn = request.guard::<BackendDbConn>().unwrap();

		let token = authservice::validar_auth(&conn, id_user, secret);

		if token.is_err() {
			return Outcome::Failure((Status::BadRequest, ()));
		}

		return Outcome::Success( token.unwrap() );
	}
}
